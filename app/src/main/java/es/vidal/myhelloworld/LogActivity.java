package es.vidal.myhelloworld;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class LogActivity extends Activity {

    private static final String DEBUG_TAG = "LogsAndroid-3";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        notify("OnCreate");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("LOG-" + LogActivity.class.getSimpleName() + " " + DEBUG_TAG, "onStop");
        notify("OnStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("LOG-" + LogActivity.class.getSimpleName() + " " + DEBUG_TAG,"onPause");
        notify("OnStop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("LOG-" + LogActivity.class.getSimpleName() + " " + DEBUG_TAG, "onResume");
        notify("OnResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("LOG-" + LogActivity.class.getSimpleName() + " " + DEBUG_TAG,"onRestart");
        notify("OnRestart");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("LOG-" + LogActivity.class.getSimpleName() + " " + DEBUG_TAG, "onStart");
        notify("OnStart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        boolean isFinishing = isFinishing();
        Log.i("LOG-" + LogActivity.class.getSimpleName() + " " + DEBUG_TAG, "onDestroy "
                + isFinishing);
        notify("OnDestroy");

    }

    @Override
    public boolean isFinishing() {
        return super.isFinishing();
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i("LOG-" + LogActivity.class.getSimpleName() + " " + DEBUG_TAG
                , "onRestoreInstanceState");
        notify("OnRestoreInstanceState");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i("LOG-" + LogActivity.class.getSimpleName() + " " + DEBUG_TAG
                , "onSaveInstanceState");
        notify("OnSaveInstanceState");
    }

    public void launchNextActivity(View view) {
        startActivity(new Intent(this, NextActivity.class));
    }

    private void notify(String eventName){

        String activityName = this.getClass().getSimpleName();

        String CHANNEL_ID = "My_lifeCycle";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){

            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID
                    ,"My Lifecycle", NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription("lifecycle events");
            notificationChannel.setShowBadge(true);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            if (notificationManager != null){
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this
                , CHANNEL_ID)
                .setContentTitle(eventName + " " + activityName)
                .setContentText(getPackageName())
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher);

        notificationManagerCompat.notify((int) System.currentTimeMillis(), notificationBuilder.build());
    }
}
